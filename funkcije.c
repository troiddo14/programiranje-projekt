#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"

FILE* otvaranjeFile() {
	FILE* fp = NULL;
	fp = fopen("korisnici1.txt", "a");
	if (fp == NULL) {
		printf("Datoteku nije moguce otvoriti.");
		return fp;
	}
	return fp;
}
int izbornik1(FILE* fp, int j) {
	int i;
	for ( i = j-1; i < j; i++) {
		printf("\nUnesite ID: ");
		scanf("%d", &(polje+i)->id);
		printf("\nUnesite ime: ");
		scanf("%s", &(polje + i)->ime);
		printf("\nUnesite prezime: ");
		scanf("%s", &(polje + i)->prezime);
		printf("\nUnesite kontakt broj: ");
		scanf("%d", &(polje + i)->kontakt);
		printf("\nUnesite mail: ");
		scanf("%s", &(polje + i)->mail);
		printf("\nUnesite marku mobitela: ");
		scanf("%s", &(polje + i)->marka);
		printf("\nUnesite model mobitela: ");
		getchar();
		fgets((polje + i)->model, 150, stdin);
		size_t ln = strlen((polje+i)->model) - 1;
		if (((polje + i)->model)[ln] == '\n')
			((polje + i)->model)[ln] = '\0';
		printf("\nUnesite opis kvara na  mobitelu: ");
		fgets((polje + i)->kvar, 150, stdin);
		size_t tn = strlen((polje + i)->kvar) - 1;
		if (((polje + i)->kvar)[tn] == '\n')
			((polje + i)->kvar)[tn] = '\0';
		printf("\nUnesite cijenu popravka mobitela: ");
		scanf("%d", &(polje + i)->cijena);
		fprintf(fp, "%d %s %s %d %s", (polje + i)->id, (polje + i)->ime, (polje + i)->prezime, (polje + i)->kontakt, (polje + i)->mail);
		fprintf(fp, " %s %s %s %d\n", (polje + i)->marka, (polje + i)->model, (polje + i)->kvar, (polje + i)->cijena);
	}
	return 0;
}
int izbornik2(FILE* fp) {
	fp = fopen("korisnici1.txt", "r");
	char line[10];

	while (fgets(line, sizeof(line), fp)) {
		printf("%s", line);
	}
	return 0;
}

int izbornik3(FILE* fp, int m) {
	fp = fopen("korisnici1.txt", "r");
	int x, f = 0;
		printf("Unesite ID: ");
		int g;
		scanf(" %d", &g);
		for (int i = 0; i < m; i++) {
			if (g == (polje + i)->id) {
				printf("\n ID je pronaden: ");
				printf("%d %s %s %d %s", (polje + i)->id, (polje + i)->ime, (polje + i)->prezime, (polje + i)->kontakt, (polje + i)->mail);
				printf(" %s %s %s %d", (polje + i)->marka, (polje + i)->model, (polje + i)->kvar, (polje + i)->cijena);
				f++;
			}

		}
		if (f == 0) {
			printf("ID nije pronaden.\n");
		}
	return 0;
}
void zamjena1(int* x, int* y)
{
	int temp = *y;
	*y = *x;
	*x = temp;
}
void zamjena2(char** a, char** b) {
	char t[100];
	strcpy(t, b);
	strcpy(b, a);
	strcpy(a, t);
}
void bubbleSort(KORISNIK* polje, int n)
{
	int i, j;
	for (i = 1; i < n-1; i++)
	{
		for (j = 1; j < n -i-1; j++)
		{
			if ((polje + j + 1)->id <(polje + j)->id)
			{
				zamjena1(&(polje + j+1)->id, &(polje+ j)->id);
				zamjena2(&(polje + j+1)->ime, &(polje + j )->ime);
				zamjena2(&(polje + j+1)->prezime, &(polje + j )->prezime);
				zamjena1(&(polje + j+1)->kontakt, &(polje + j )->kontakt);
				zamjena2(&(polje + j+1)->mail, &(polje + j)->mail);
				zamjena2(&(polje + j+1)->marka, &(polje + j )->marka);
				zamjena2(&(polje + j+1)->model, &(polje + j )->model);
				zamjena2(&(polje + j+1)->kvar, &(polje + j )->kvar);
				zamjena1(&(polje + j+1)->cijena, &(polje + j )->cijena);
			}
		}
	}
}

void printArray(KORISNIK* polje, int m)
{
	int i;
	bubbleSort(polje, m);
	for (i = 0; i <m-1; i++)
	{
		printf("Korisnik: %d %s %s %d %s", (polje + i)->id, (polje + i)->ime, (polje + i)->prezime, (polje + i)->kontakt, (polje + i)->mail);
		printf(" %s %s %s %d \n", (polje + i)->marka, (polje + i)->model, (polje + i)->kvar, (polje + i)->cijena);
	}
}
