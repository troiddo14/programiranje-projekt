
#ifndef HEADER_H
#define HEADER_H

typedef struct korinsik {
	int id;
	char ime[20];
	char prezime[20];
	int kontakt;
	char mail[30];
	char marka[15];
	char model[15];
	char kvar[150];
	int cijena;
}KORISNIK;

KORISNIK polje[10000];

FILE* otvaranjeFile();
int izbornik1(FILE* fp,int j);
int izbornik2(FILE* fp);
int izbornik3(FILE* fp,int m);
void printArray(KORISNIK* polje, int m);
void bubbleSort(KORISNIK* polje, int n);
void zamjena2(char** a, char** b);
void zamjena1(int* x, int* y);

#endif //HEADER_H