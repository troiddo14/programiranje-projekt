#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"

int main() {

	int m = 1;
	odabir:
	printf("-------Izbornik-------\n");
	printf("Za unos korisnika odaberite 1\n");
	printf("Za ispis svih korisnika odaberite 2\n");
	printf("Za pretrazivanje korisnika odaberite 3\n");
	printf("Za sortiranje korisnika odaberite 4\n");
	printf("Za izlaz iz programa stisnite 5.\n");
	int n;
	
	FILE * fp = otvaranjeFile();
	
	scanf("%d", &n);
	switch (n) {
		int h = sizeof((polje + m)->id) / sizeof(int);
	case 1:
		izbornik1(fp,m);
		fclose(fp);
		m++;
		printf("\n");
		goto odabir;
	case 2:
		izbornik2(fp);
		printf("\n");
		goto odabir;
	case 3:
		izbornik3(fp,m);
		printf("\n");
		goto odabir;
	case 4:
		printf("Soritrani korisnici: \n ");
		printArray(polje, m);
		printf("\n");
		goto odabir;
	default:
		printf("Izasli ste iz programa.");
		break;
	}
	return 0;
}
